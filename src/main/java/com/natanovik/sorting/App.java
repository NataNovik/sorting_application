package com.natanovik.sorting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <strong>Class App</strong> - contains entry point of app.
 */
public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);

    /**
     * <strong>main</strong> method calls method <strong>sort</strong>.
     * The result of <strong>sort</strong> method work will be print on the screen.
     *
     * @param args arguments
     */
    public static void main(String[] args) {
        try {
            ArraySorter arraySorter = new ArraySorter();
            String sortedArray = arraySorter.sortArray(args);
            logger.info("Display sorted numbers in ascending order: {}", sortedArray);
        } catch (IllegalArgumentException exception) {
            logger.error(exception.getMessage());
        }
    }
}
