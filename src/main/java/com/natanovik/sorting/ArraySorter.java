package com.natanovik.sorting;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * <strong>ArraySorter</strong> class.
 * Contains methods for sorting numbers.
 */
public class ArraySorter {
    private final Logger logger = LoggerFactory.getLogger(ArraySorter.class);


    /**
     * The method <strong>sortingArray</strong>.
     * Accepts an array of string arguments from the console:
     * converts the resulting string array to an integer,
     * and sorts the values of the array in ascending order
     *
     * @param array An array of values to be sorted.
     * @return String of integers in ascending order.
     */
    public String sortArray(String[] array) {
        validate(array);
        int[] inputArray = parseArray(array);
        Arrays.sort(inputArray);
        StringBuilder resultBuilder = new StringBuilder();
        for (int elementValue : inputArray) {
            resultBuilder.append(elementValue).append(" ");
        }
        logger.info("The sorted array of integers is now represented as a string.");
        return resultBuilder.toString().trim();
    }

    private void validate(String[] inputArray) {
        if (inputArray == null || inputArray.length == 0) {
            throw new IllegalArgumentException("Elements are not defined");
        }
        if (inputArray.length > 10) {
            throw new IllegalArgumentException("No more then 10 elements can sorted");
        }

        if (!isIntegerArray(inputArray)) {
            throw new IllegalArgumentException("Input values must be integers");
        }
    }

    private boolean isIntegerArray(String[] str) {
        try {
            for (String s : str) {
                Integer.parseInt(s);
            }
        } catch (NumberFormatException exception) {
            return false;
        }
        return true;
    }

    private int[] parseArray(String[] strArr) {
        int[] inputArray = new int[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            inputArray[i] = Integer.parseInt(strArr[i]);
        }
        return inputArray;
    }
}
