package com.natanovik.sorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;


@RunWith(Parameterized.class)
public class ArraySorterParameterizedTests {
    private final ArraySorter arraySorter = new ArraySorter();
    private final String inputNumbers;
    private final String outputNumbers;

    public ArraySorterParameterizedTests(String inputNumbers, String outputNumbers) {
        this.inputNumbers = inputNumbers;
        this.outputNumbers = outputNumbers;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"1 7 4 2 3 -9", "-9 1 2 3 4 7"},
                {"2", "2"},
                {"0 1 3 4 6 7 9 8 -1 2", "-1 0 1 2 3 4 6 7 8 9"},
        });
    }

    @Test
    public void sortingTest() {
        String[] inputArray = inputNumbers.split(" ");
        String result = arraySorter.sortArray(inputArray);
        assertEquals(outputNumbers, result);
    }
}
