package com.natanovik.sorting;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ArraySorterTests {
    private final ArraySorter arraySorter = new ArraySorter();

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionIfArrayIsNull() {
        arraySorter.sortArray(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionWhenArrayIsEmpty() {
        String[] array = {};
        arraySorter.sortArray(array);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionWhenMoreThanTenElementsInArray() {
        String[] arrayInput = {"-1", "2", "4", "5", "8", "78", "-899", "0", "-2", "12", "47", "14"};
        arraySorter.sortArray(arrayInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionWhenNotIntegerInArray() {
        String[] arrayInput = {"2", "-9", "!"};
        arraySorter.sortArray(arrayInput);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionWhenDoubleInArray() {
        String[] arrayInput = {"2", "-9", 5.5 + ""};
        arraySorter.sortArray(arrayInput);
    }

    @Test
    public void shouldReturnTheSameArrayAfterSortingWhenArrayHasOnlyOneElement() {
        String[] arrayInput = {"3"};
        String actualResult = arraySorter.sortArray(arrayInput);
        assertEquals("3", actualResult);
    }

    @Test
    public void shouldSortArrayWhenItContainsTenElements() {
        String[] arrayInput = {"-1", "2", "4", "5", "8", "78", "-899", "0", "-2", "12"};
        String actualResult = arraySorter.sortArray(arrayInput);
        assertEquals("-899 -2 -1 0 2 4 5 8 12 78", actualResult);
    }

    @Test
    public void shouldReturnTheSameArrayAfterSortingWhenArrayHaveValuesOfElementsInAscendingOrder() {
        String[] arrayInput = {"1", "2", "3", "5"};
        String actualResult = arraySorter.sortArray(arrayInput);
        assertEquals("1 2 3 5", actualResult);
    }

    @Test
    public void shouldSortArrayWhenItContainsValidLength() {
        String[] arrayInput = {"-1", "2", "35", "5", "-8", "78"};
        String actualResult = arraySorter.sortArray(arrayInput);
        assertEquals("-8 -1 2 5 35 78", actualResult);
    }
}
